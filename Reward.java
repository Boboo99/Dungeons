package net.Boboo99.Dungeon;

import org.json.JSONObject;

public class Reward {
    private int value;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    Reward(int value)
    {
        this.value = value;
    }

    Reward() {

    }

    public JSONObject toJsonObject()
    {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("value",value);
        return jsonObject;
    }

    public Reward fromJsonObject(JSONObject object)
    {
        value = object.getInt("value");
        return this;
    }
}
