package net.Boboo99.Dungeon;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.json.JSONObject;

import java.util.*;

public class Mob {
    private HashMap<String,EntityType> mobNameEntityTypeMap = new HashMap<>();


    private void initializeMobNameEntityTypeMap()
    {
        mobNameEntityTypeMap.put("zombie",EntityType.ZOMBIE);
        mobNameEntityTypeMap.put("pig_zombie",EntityType.PIG_ZOMBIE);
        mobNameEntityTypeMap.put("villager_zombie",EntityType.ZOMBIE_VILLAGER);
        mobNameEntityTypeMap.put("spider",EntityType.CAVE_SPIDER);
        mobNameEntityTypeMap.put("skeleton",EntityType.SKELETON);
        mobNameEntityTypeMap.put("witch",EntityType.WITCH);
    }

    Mob(World world)
    {
        this.world = world;
    }

    Mob(String name, int health, boolean boss, int reward, Location location)
    {
        initializeMobNameEntityTypeMap();

        this.name = name;
        this.health = health;
        this.isBoss = boss;
        this.reward = new Reward(reward);
        this.location = location;
    }

    private String name;
    private Entity entity;
    private int health;
    private Location location;
    private boolean isBoss;
    private Reward reward;
    private World world;

    public Entity getEntity() {
        return entity;
    }

    public void setEntity(Entity entity) {
        this.entity = entity;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public boolean isBoss() {
        return isBoss;
    }

    public void setBoss(boolean boss) {
        isBoss = boss;
    }

    public Reward getReward() {
        return reward;
    }

    public void setReward(Reward reward) {
        this.reward = reward;
    }

    public void spawn()
    {
        entity = world.spawnEntity(location,mobNameEntityTypeMap.get(name));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public JSONObject toJsonObject()
    {
        JSONObject jsonObject = new JSONObject();
        JSONObject mobLocationJsonObject = new JSONObject();

        mobLocationJsonObject.put("x",location.getX());
        mobLocationJsonObject.put("y",location.getY());
        mobLocationJsonObject.put("z",location.getZ());

        jsonObject.put("name",name);
        jsonObject.put("health",health);
        jsonObject.put("location",mobLocationJsonObject);
        jsonObject.put("boss",isBoss);
        jsonObject.put("reward", reward.toJsonObject());

        return jsonObject;
    }

    public Mob fromJsonObject(JSONObject object)
    {
        JSONObject locationJson = object.getJSONObject("location");

        name = object.getString("name");
        health = object.getInt("health");

        location = new Location(world,locationJson.getDouble("x"),locationJson.getDouble("y"),locationJson.getDouble("z"));

        isBoss = object.getBoolean("boss");

        reward = new Reward().fromJsonObject(object.getJSONObject("reward"));

        return this;
    }

}
