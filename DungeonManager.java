package net.Boboo99.Dungeon;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;

public class DungeonManager {
    static ArrayList<Dungeon> dungeons = new ArrayList<>();

    private Dungeon currentDungeon;
    private Player currentPlayer;

    public static void loadDungeonsFromFile(World world)
    {
        try {
            if(!Files.exists(Paths.get("dungeons.json")))
                return;

            for (String jsonLine : Files.readAllLines(Paths.get("dungeons.json"), Charset.defaultCharset())) {
                dungeons.add(new Dungeon(world).fromJsonObject(new JSONObject(jsonLine)));
            }
            Files.delete(Paths.get("dungeons.json"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveDungeonsToFile()
    {
        try {
            for (Dungeon d: dungeons) {
                Files.write(Paths.get("dungeons.json"),(d.toJsonObject() + "\n").getBytes(), StandardOpenOption.CREATE,StandardOpenOption.APPEND);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addNewDungeon(Player player,String dungeonName,int finishingReward)
    {
        currentDungeon = new Dungeon(player.getWorld());
        currentPlayer = player;
        currentDungeon.setId(dungeons.size());
        currentDungeon.setName(dungeonName);
        currentDungeon.setEntryPoint(player.getLocation());
        currentDungeon.setReward(new Reward(finishingReward));
    }

    public void addNewMob(Mob mob) {

        if(currentDungeon == null)
            Bukkit.broadcastMessage("Create the Dungeon before adding a new Mob");

        currentDungeon.getMobs().add(mob);
    }

    public void spawnMobs() {
        for (Mob m : currentDungeon.getMobs()) {
            m.spawn();
        }
    }

    public void finishNewDungeon() {
        currentDungeon.setEscapePoint(currentPlayer.getLocation());
        dungeons.add(currentDungeon);
        currentDungeon = null;
        currentPlayer = null;
    }

    public Dungeon getCurrentDungeon() {

        if(currentDungeon == null)
            Bukkit.broadcastMessage("You are not editing any dungeon at the moment");

        return currentDungeon;
    }

    public static void listDungeons() {
        dungeons.forEach(d -> Bukkit.broadcastMessage(d.getName()));
    }

    public static void spawnDungeon(String dungeonName) {
        dungeons.forEach(dungeon -> {
            if (dungeon.getName().equals(dungeonName))
                dungeon.create();
        });
    }
}
