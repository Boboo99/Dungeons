package net.Boboo99.Dungeon;

import org.bukkit.Location;
import java.util.ArrayList;

import org.bukkit.World;
import org.json.*;

public class Dungeon {

    private int id;
    private String name;
    private Location entryPoint;
    private Location escapePoint;
    private ArrayList<Mob> mobs = new ArrayList<>();
    private Reward reward;
    private World world;

    Dungeon(World world) {
        this.world = world;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getEntryPoint() {
        return entryPoint;
    }

    public void setEntryPoint(Location entryPoint) {
        this.entryPoint = entryPoint;
    }

    public Location getEscapePoint() {
        return escapePoint;
    }

    public void setEscapePoint(Location escapePoint) {
        this.escapePoint = escapePoint;
    }

    public ArrayList<Mob> getMobs() {
        return mobs;
    }

    public void setMobs(ArrayList<Mob> mobs) {
        this.mobs = mobs;
    }

    public Reward getReward() {
        return reward;
    }

    public void setReward(Reward reward) {
        this.reward = reward;
    }

    public JSONObject toJsonObject()
    {
        JSONObject mainJsonObject = new JSONObject();
        JSONObject startPointJsonObject = new JSONObject();
        JSONObject endPointJsonObject = new JSONObject();
        JSONArray mobJsonArray = new JSONArray();

        mainJsonObject.put("id",id);
        mainJsonObject.put("name",name);

        startPointJsonObject.put("x",entryPoint.getX());
        startPointJsonObject.put("y",entryPoint.getY());
        startPointJsonObject.put("z",entryPoint.getZ());

        endPointJsonObject.put("x",escapePoint.getX());
        endPointJsonObject.put("y",escapePoint.getY());
        endPointJsonObject.put("z",escapePoint.getZ());

        mainJsonObject.put("entrypoint",startPointJsonObject);
        mainJsonObject.put("escapepoint",endPointJsonObject);


        for (Mob m :
                mobs) {
            mobJsonArray.put(m.toJsonObject());
        }

        mainJsonObject.put("mobs",mobJsonArray);
        mainJsonObject.put("reward",reward.toJsonObject());

        return mainJsonObject;
    }

    public Dungeon fromJsonObject(JSONObject object) {
        id = object.getInt("id");
        name = object.getString("name");

        JSONObject startPoint = object.getJSONObject("entrypoint");
        JSONObject endPoint = object.getJSONObject("escapepoint");


        entryPoint = new Location(world,startPoint.getDouble("x"),startPoint.getDouble("y"),startPoint.getDouble("z"));
        escapePoint = new Location(world,endPoint.getDouble("x"),endPoint.getDouble("y"),endPoint.getDouble("z"));

        JSONArray mobsJson = object.getJSONArray("mobs");

        for(int i = 0;i<mobsJson.length();i++)
            mobs.add(new Mob(world).fromJsonObject(mobsJson.getJSONObject(i)));

        reward = new Reward().fromJsonObject(object.getJSONObject("reward"));

        return this;
    }

    public void create()
    {
        mobs.forEach(m -> m.spawn());
    }
}
