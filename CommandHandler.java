package net.Boboo99.Dungeon;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandHandler {


    CommandHandler() {
    }

    private DungeonManager dungeonManager;
    public boolean handleCommand(CommandSender sender, Command command,String label,String[] args) {

        if(label.equalsIgnoreCase("create_dungeon")) {
            dungeonManager = new DungeonManager();
            dungeonManager.addNewDungeon((Player)sender,args[0],Integer.parseInt(args[1]));
        }
        if(label.equalsIgnoreCase("add_mob")) {
            dungeonManager.addNewMob(new Mob(args[0], Integer.parseInt(args[1]), Boolean.parseBoolean(args[2]), Integer.parseInt(args[3]),((Player)sender).getLocation()));
        }
        if(label.equalsIgnoreCase("spawn_mobs")) {
            dungeonManager.spawnMobs();
        }
        if(label.equalsIgnoreCase("finish_dungeon")) {
            dungeonManager.finishNewDungeon();
        }
        if(label.equalsIgnoreCase("get_current_dungeon")) {
            Bukkit.broadcastMessage(dungeonManager.getCurrentDungeon().getName());
        }
        if(label.equalsIgnoreCase("list_dungeons")) {
            DungeonManager.listDungeons();
        }
        if(label.equals("spawn_dungeon")) {
            DungeonManager.spawnDungeon(args[0]);
        }
        return true;
    }
}
